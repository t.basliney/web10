$(document).ready(function(){
  $('.myslick-slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      centerMode: true,
      dots: true,
      arrows: true,
      adaptiveHeight: true,
      lazyLoad: 'ondemand',
      responsive: [
        {
          breakpoint: 800,
          settings: {
            arrows: false,
            centerMode: false,
            slidesToShow: 1,
            arrows: true,
            dots: false
          }
        }]
    });
});
$(document).ready(function(){
$('.cert-slider').slick({
  slidesToShow: 3,
  infinite: true,
  prevArrow: '<i class="material-icons prev2">keyboard_arrow_left</i>',
  nextArrow: '<i class="material-icons next2">keyboard_arrow_right</i>',
  arrows: false,
  dots: false,
  responsive: [
    {
      breakpoint: 800,
      settings: {
        arrows: true,
        slidesToShow: 1
      }
    }]
});
});
$(".butt2").click(function(){
    
$("html").addClass("openNav");

});
$(".bcanc").click(function(){

$("html").removeClass("openNav");

});

let checkForm = document.getElementById("form_link");
let fields = document.getElementById("fields");
let first_button = document.getElementById("first_button");
let second_button = document.getElementById("second_button");
form_non_active();

function form_active() {
first_button.style.display = "none";
second_button.style.display = "block";
fields.style.display = "block";
getDataStorage();
}

function form_non_active() {
first_button.style.display = "block";
second_button.style.display = "none";
fields.style.display = "none";
}

$(".add_button").click(function(){
form_active();
});

$(".slbutt").click(function(){
form_active();
checkForm.scrollIntoView({block: "center", behavior: "smooth"});
history.pushState(1, "title", "#form_link");
});

window.onpopstate = function(event) {
if(event.state==1) {
  form_active();
}
else {
  form_non_active();
}
};

function updateStorage() {
localStorage.setItem("name", document.getElementById("name").value);
localStorage.setItem("email", document.getElementById("email").value);
localStorage.setItem("comment", $("#comment").val());
}

function getDataStorage() {
document.getElementById("name").value = localStorage.getItem("name");
document.getElementById("email").value = localStorage.getItem("email");
$("#comment").val(localStorage.getItem("comment"));
}

window.addEventListener("DOMContentLoaded", function (event) {
let profit = document.getElementById("form_link");
profit.addEventListener("input", function(event) {
  updateStorage();
});
});